<?php include "partial/header.php" ?>
  
    <div class="container">
    <div>
    <?php include 'partial/messages.php' ?>
    </div>
    <div class="row clear">
     
          <div class="col-md-12">
              <h1 class="text-center">URHome</h1>
          </div>
          <div class="col-md-8">
            <p class="padding-t-lg">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt quas, neque mollitia nihil dolor similique tempore suscipit. Corrupti, nesciunt quibusdam omnis, modi ipsam eos id dolorem aliquam nihil natus vel.
            </p>
            <div class="margin-lg text-center">
            <a class='btn btn-primary' href='signin.php'>Sign in</a>
            <a class='btn btn-link' href="index.php">home</a>
        </div>
          </div>
    <div class="col-md-4 bg-w">
        <div class="margin-lg">
        <h4 class="text-left">Inscription</h4>
        <form action="signup_server.php" method='post' class='form'>
            
            <div class='form-group'> 
                <label for="first_name">Nom</label>    
                <input type="text" name="first_name" id="first_name" placeholder='Nom' class='form-control'>
            </div>
            <div class='form-group'>
                <label for="last_name">Prénom</label>
              <input type="text" id="last_name" name="last_name" placeholder='prénom' class='form-control'>
            </div>
            <hr>
            <div class='form-group'> 
                <label for="email">Address mail</label>    
                <input type="email" name="email" id="email" placeholder='Address mail' class='form-control'>
            </div>
            <div class='form-group'>
                <label for="phone">Phone</label>
              <input type="tel" id="phone" name="phone" placeholder='Numéro de téléphone' class='form-control'>
            </div>
            <hr>

            <div class='form-group'>
              <label for="mot_de_passe">Mot de passe</label>
              <input type="password" name='password' id='mot_de_passe' class='form-control' require>
            </div>
            <div class='form-group'>
              <label for="confirmation_mot_de_passe">Confirmation</label>
              <input type="password" name='confirmation_password' id="confirmation_mot_de_passe" class='form-control' require>
            </div>
          <hr>
          <div class='form-group'>
          <input type="radio" name="account_type" value="client"> Client
          <input type="radio" name="account_type" value="artisant"> Artisan
          </div>
            <div class='form-group'>
              <input type="submit" value="Inscription" class="btn btn-primary b-r-0">
            </div>         
          </form>
        </div>
    </div>
    </div>
    </div>
    <?php include "partial/footer.php" ?>