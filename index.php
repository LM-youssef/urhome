<?php include "partial/header.php"; 
// phpinfo();
?>
<div class="container">
    <div class="row clear">
    <div class="col-md-8 col-md-offset-2">
        <h1 class="text-center">URHome</h1>

        <div class="margin-lg text-center">
            <a class='btn btn-link' href="signin.php">Sign in</a>
            <a class='btn btn-primary' href="signup.php">Sign up</a>
        </div>
    </div>
    </div>
</div>
<?php include "partial/footer.php" ?>
