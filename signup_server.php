<?php
// Start the session
session_start();

include "entity/Database.php";
$db = Database::getInstance();
$bdd = $db->getConnection();

?>
<?php
//  check if all required data were sent to the server.
$is_all_field_sent = true;
$fields = [ 'first_name',
            'last_name',
            'password',
            'confirmation_password',            
            'email',
            'account_type',            
            'phone'];

    foreach ($fields as $key => $value) {
        if(!isset($_POST[$value]) || $_POST[$value] == "" )
        {
            $is_all_field_sent = false;   
            break;
        }
    }
if($is_all_field_sent == false){
//  return to inscription page and show error message
$_SESSION['message']['error'] =  "tout les champs doivent avoir une valeur, Merci";
header('Location: /urhome/signup.php');
die();
}
else{
  
    // fill in $user object by data;
    foreach ($fields as $key => $value) {
        $user[$value] = addslashes($_POST[$value]);
    }
    // check if email and phone did not already exist
    $user['password'] = MD5($user['password'].$user['email']);
    $query_check_email = "SELECT * FROM user where email = '{$user['email']}'" ;
    $query_check_phone = "SELECT * FROM user where phone_number = '{$user['phone']}'" ;


    $user_by_email = $bdd->query($query_check_email);
    $user_by_phone = $bdd->query($query_check_phone);
    if( ($user_by_email != FALSE &&  $user_by_email->rowCount() != 0 ) || ( $user_by_phone != FALSE && $user_by_phone->rowCount() != 0) ) {
        
        if( ($user_by_email != FALSE &&  $user_by_email->rowCount() != 0 )) {
            $_SESSION['message']['error'] = "Le numéro de téléphone est déja utilisé. ";
        }
        
        if( ( $user_by_phone != FALSE && $user_by_phone->rowCount() != 0)) {
            $_SESSION['message']['error'] = "L'address email est déja utilisée. ";
        }
       
        header('Location: /urhome/signup.php');
        die();
    } 
    else {
        $query_insert = "INSERT INTO user (first_name, last_name, email, phone_number, password, category) VALUES ('{$user['first_name']}', '{$user['last_name']}', '{$user['email']}', '{$user['phone']}', '{$user['password']}', '{$user['account_type']}')" ;
        try {
            $bdd->query($query_insert);
        }catch(Exception $e){
        $_SESSION['message']['error'] = "Insertion problem";
        header('Location: /urhome/signup.php');            
        }
        $_SESSION['message']['notify'] = "Insertion success";
        header('Location: /urhome/signin.php');
        die();
    }
}

?>