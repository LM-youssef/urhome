-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;

-- ---
-- Table 'user'
-- 
-- ---

DROP TABLE IF EXISTS `user`;
		
CREATE TABLE `user` (
  `user_id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `first_name` VARCHAR(60) NULL DEFAULT NULL,
  `last_name` VARCHAR(60) NULL DEFAULT NULL,
  `phone_number` VARCHAR(20) NULL DEFAULT NULL,
  `email` VARCHAR(60) NULL DEFAULT NULL,
  `category` VARCHAR(30) NULL DEFAULT NULL,
  `password` VARCHAR(70) NULL DEFAULT NULL,
  `created_at` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`)
);

-- ---
-- Table 'support'
-- 
-- ---

DROP TABLE IF EXISTS `support`;
		
CREATE TABLE `support` (
  `support_id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `label` VARCHAR(60) NULL DEFAULT NULL,
  `type` VARCHAR(60) NULL DEFAULT NULL,
  `link` VARCHAR(60) NULL DEFAULT NULL,
  `created_at` DATE NULL DEFAULT NULL,
  `offer_id` INTEGER NULL DEFAULT NULL,
  PRIMARY KEY (`support_id`)
);

-- ---
-- Table 'offer'
-- 
-- ---

DROP TABLE IF EXISTS `offer`;
		
CREATE TABLE `offer` (
  `offer_id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `due_date` DATE NULL DEFAULT NULL,
  `title` VARCHAR(60) NULL DEFAULT NULL,
  `description` MEDIUMTEXT NULL DEFAULT NULL,
  `status` VARCHAR(60) NULL DEFAULT NULL,
  `max_price` INTEGER NULL DEFAULT NULL,
  `created_at` DATE NULL DEFAULT NULL,
  `user_id` INTEGER NULL DEFAULT NULL,
  PRIMARY KEY (`offer_id`)
);

-- ---
-- Table 'response_to_offer'
-- 
-- ---

DROP TABLE IF EXISTS `response_to_offer`;
		
CREATE TABLE `response_to_offer` (
  `response_to_offer_id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `descriptionn` MEDIUMTEXT NULL DEFAULT NULL,
  `price` INTEGER NULL DEFAULT NULL,
  `execution_date` INTEGER NULL DEFAULT NULL,
  `created_at` DATE NULL DEFAULT NULL,
  `offer_id` INTEGER NULL DEFAULT NULL,
  `user_id` INTEGER NULL DEFAULT NULL,
  PRIMARY KEY (`response_to_offer_id`)
);

-- ---
-- Foreign Keys 
-- ---

ALTER TABLE `support` ADD FOREIGN KEY (offer_id) REFERENCES `offer` (`offer_id`);
ALTER TABLE `offer` ADD FOREIGN KEY (user_id) REFERENCES `user` (`user_id`);
ALTER TABLE `response_to_offer` ADD FOREIGN KEY (offer_id) REFERENCES `offer` (`offer_id`);
ALTER TABLE `response_to_offer` ADD FOREIGN KEY (user_id) REFERENCES `user` (`user_id`);

-- ---
-- Table Properties
-- ---

-- ALTER TABLE `user` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `support` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `offer` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `response_to_offer` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ---
-- Test Data
-- ---

-- INSERT INTO `user` (`user_id`,`first_name`,`last_name`,`phone_number`,`email`,`category`,`password`,`created_at`) VALUES
-- ('','','','','','','','');
-- INSERT INTO `support` (`support_id`,`label`,`type`,`link`,`created_at`,`offer_id`) VALUES
-- ('','','','','','');
-- INSERT INTO `offer` (`offer_id`,`due_date`,`title`,`description`,`status`,`max_price`,`created_at`,`user_id`) VALUES
-- ('','','','','','','','');
-- INSERT INTO `response_to_offer` (`response_to_offer_id`,`descriptionn`,`price`,`execution_date`,`created_at`,`offer_id`,`user_id`) VALUES
-- ('','','','','','','');