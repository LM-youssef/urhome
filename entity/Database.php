<?php
/*
* Mysql database class - only one connection alowed
*/
class Database {
	private $_connection;
	private static $_instance; //The single instance
	private $_host = "localhost";
	private $_username = "root";
	private $_password = "architect";
	private $_database = "urhome";

	/*
	Get an instance of the Database
	@return Instance
	*/
	public static function getInstance() {
		if(!self::$_instance) { // If no instance then make one
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	// Constructor
	private function __construct() {
      try
    {
      // On se connecte à MySQL
      $this->_connection = new PDO("mysql:host={$this->_host};dbname={$this->_database};charset=utf8", $this->_username, $this->_password);
    }
    catch(Exception $e)
    {
      // En cas d'erreur, on affiche un message et on arrête tout
      die('Erreur : '.$e->getMessage());
    }
	}

	// Magic method clone is empty to prevent duplication of connection
	private function __clone() { }

	// Get mysql connection
	public function getConnection() {
		return $this->_connection;
	}
}
?>