<?php include "partial/header.php"; 
$connected_user = $_SESSION['connected_user']; 
?>

<div class="container">
  <div class="row clear">
    <div class="col-md-12">
      <h2>URHome</h2>
    </div>
    <div class="col-md-4">
      <div class="padding-xs bg-w">
        <h3>user</h3>
        <div class="first-name">
          Nom:
          <b>
            <?php echo $connected_user['first_name'] ?>
          </b>
        </div>
        <div class="last-name">
          Prénom:
          <b>
            <?php echo $connected_user['last_name'] ?>
          </b>
        </div>
        <div class="email">
          @:
          <b>
            <?php echo $connected_user['email'] ?>
          </b>
        </div>
        <div class="phone">
          Tel:
          <b>
            <?php echo $connected_user['phone_number'] ?>
          </b>
        </div>
      </div>
      <div class="bg-w padding-xs m-t-xs">
        <b>
          <?php echo $connected_user['category'] ?> </b>
      </div>

      <div class="bg-w padding-xs m-t-xs">
        <a href="signout_server.php">Déconnexion</a>
      </div>
    </div>
    <div class="col-md-8">
      <form action="" class="bg-w padding-xs" method="post">
        <div class="form-group">
          <input type="text" placeholder="Titre" name="title" class="form-control">
        </div>

        <div class="form-group">
          <input type="text" placeholder="Prix" name="max_price" class="form-control">
        </div>

        <div class="form-group">
          <input type="text" placeholder="Date limit" name="due_date" class="form-control">
        </div>  
        <div class="form-group">
          <textarea class="form-control" placeholder="description"></textarea>
        </div>        

        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Valider">
        </div>
      </form>

      <div class="padding-xs bg-w m-t-xs">
          <span class="pull-right label label-info">
              Activé
            </span>
        <h4> <a href="offer.php?id=1"> Lorem ipsum dolor </a>
        </h4>
        <p>sit amet consectetur adipisicing elit. Assumenda odit consectetur natus autem libero, mollitia vero minima tenetur dolorem dolores dolorum, maiores odio reprehenderit doloribus consequuntur impedit placeat, quas enim!</p>
       
        <div class="text-right  m-t-xs">
          <hr>
          <span class="text-primary">
            <b>10000 DH</b>
          </span>
          |
          <span class="">
              <b>12/12/1921 <span class="fa fa-clock-o"></span> </b>
            </span>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include "partial/footer.php" ?>